# SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0
---
apiVersion: "kyverno.io/v1"
kind: "ClusterPolicy"
metadata:
  name: "disallow-container-sock-mounts"
  annotations:
    policies.kyverno.io/title: "Disallow CRI socket mounts"
    policies.kyverno.io/subject: "Pod"
    policies.kyverno.io/description: >-
      Container daemon socket bind mounts allow access to the container engine on the node.
      This access can be used for privilege escalation and to manage containers outside of Kubernetes, and hence should
      not be allowed.
      This policy validates that the sockets used for CRI engines Docker, Containerd, and CRI-O are not used.
      In addition to or replacement of this policy, preventing users from mounting the parent directories
      (/var/run and /var) may be necessary to completely prevent socket bind mounts.
spec:
  background: true
  rules:
    - name: "validate-docker-sock-mount"
      match:
        any:
          - resources:
              kinds:
                - "Pod"
      validate:
        message: "Use of the Docker Unix socket is not allowed."
        anyPattern:
          - spec:
              =(volumes):
                - =(hostPath):
                    path: "!/var/run/docker.sock"
          - spec:
              =(volumes):
    - name: "validate-containerd-sock-mount"
      match:
        any:
          - resources:
              kinds:
                - "Pod"
      validate:
        message: "Use of the Containerd Unix socket is not allowed."
        anyPattern:
          - spec:
              =(volumes):
                - =(hostPath):
                    path: "!/var/run/containerd/containerd.sock"
          - spec:
              =(volumes):
    - name: "validate-crio-sock-mount"
      match:
        any:
          - resources:
              kinds:
                - "Pod"
      validate:
        message: "Use of the CRI-O Unix socket is not allowed."
        anyPattern:
          - spec:
              =(volumes):
                - =(hostPath):
                    path: "!/var/run/crio/crio.sock"
          - spec:
              =(volumes):
    - name: "validate-dockerd-sock-mount"
      match:
        any:
          - resources:
              kinds:
                - "Pod"
      validate:
        message: "Use of the Docker CRI socket is not allowed."
        anyPattern:
          - spec:
              =(volumes):
                - =(hostPath):
                    path: "!/var/run/cri-dockerd.sock"
          - spec:
              =(volumes):
